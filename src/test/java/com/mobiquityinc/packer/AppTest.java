package com.mobiquityinc.packer;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
    public void testPackSucess() throws APIException {
    	
    	String sExpectedReturn =  "4\n" + "-\n" + "2,7\n" + "8,9\n";
    	
    	String sDoneReturn= Packer.pack("C:\\Users\\vinicius.guimaraes\\Documents\\packageproblem.txt");
    	
    	assertEquals(sExpectedReturn, sDoneReturn);
    }    

    
    public void testPackException() throws APIException {   	
		
    	BufferedReader buffered = null;
		
    	try {

			File file = new File("absolutePath");
			
			buffered = new BufferedReader(new FileReader(file));
				
			} catch (FileNotFoundException fileNotFoundException) {
				
				fileNotFoundException.printStackTrace();
			}

    }
}
