package com.mobiquityinc.packer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 	Package	Challenge
 *  Packer Class
 *
 */
public class Packer {
	
	public static String pack(String absolutePath) throws APIException {
		
		BufferedReader buffered = null;
		List<Thing> thingList = new ArrayList<Thing>();
		String line;    
	    StringBuilder builder = new StringBuilder();
		
		try {			
			File file = new File(absolutePath);
			
			buffered = new BufferedReader(new FileReader(file));
				
			} catch (FileNotFoundException fileNotFoundException) {
				
				fileNotFoundException.printStackTrace();
			}
		    
		    try {
		    	
				while ((line = buffered.readLine()) != null) {
						
						String resultString = "";

				        String[] items = line.split(" : ");
				        
				        int weightLimit = Integer.parseInt(items[0]);
				        
				        String[] itemsFormat = items[1].split(" ");
				        
				        if(itemsFormat.length > 0){
				        
				        thingList.clear();
				        
				        parserThings(itemsFormat, thingList, weightLimit);		    

					    double maxCost=0;
					    double maxWeight=0;
				        	
				        for(int index = 1; index <= thingList.size(); index++)
					    {
				        		
					    	String resultCombination = getCombination(thingList, index, weightLimit);
					    	
					    	String[] resultFormatList = resultCombination.split(",");
					    	
					    	double cost = Double.parseDouble(resultFormatList[resultFormatList.length-2]);
					    	double weight = Double.parseDouble(resultFormatList[resultFormatList.length-1]);
					    	
					    	if((cost == maxCost && weight < maxWeight) || cost > maxCost) {			    	
						    		maxCost = cost;
						    		maxWeight = weight;
						    		resultString = resultCombination;			    			
					    	}   	
					    	
					    }		    

					    String[] finalFormatList = resultString.split(",");
					    String outPutString = "";
					    
					    for(int index = 0; index < finalFormatList.length-2; index++) {
					    
					    	outPutString += thingList.get(Integer.parseInt(finalFormatList[index])).getId()+",";
					    }
					    if(outPutString.equals(""))
					    	builder.append("-\n");
					    else
					    	builder.append(outPutString.substring(0, outPutString.length()-1)).append("\n");
				        }else
				        	builder.append("-\n");

				}

			} catch (IOException ioException) {
				ioException.printStackTrace();
			}	
		
		return builder.toString();
		
	}
	
	public static void parserThings(String[] items, List<Thing> thingList, double maxWeight){
		
		Optional.ofNullable(Arrays.asList(items)).ifPresent(l -> l.stream().forEach(i -> {
			String[] item = i.substring(1, i.length()-1).split(",");
			double weight = Double.parseDouble(item[1]);
			
	    	if(weight <= maxWeight) {
		    	int id = Integer.parseInt(item[0]);
		    	double cost = Double.parseDouble(item[2].substring(1,item[2].length()));

		        Thing thing = new Thing(id, weight, cost);
		        thingList.add(thing);
	    	}

		}));
	}

static String getCombination(List<Thing> thingList, int index,int NmaxWeight){
	
	int indexSolution = 0;
	String returnData = "";
	double maxWeight = 0;
	double maxCost = 0;
    int[] data = new int[index];
    
    List<Integer> indexList = new ArrayList<Integer>();
    int[] arr = new int[thingList.size()];
    
    for(int indexSize = 0; indexSize < thingList.size(); indexSize++) {
    
    	arr[indexSize] = indexSize;
    }
    
    makeCombination(arr, data, indexList, 0, 0);
    
    for(int i = 0;i <= indexList.size()-index; i += index) {
    
    	double somWeight = 0;
    	double somCost = 0;
    	
    	for(int j=0; j<index; j++)
    	{
    		somWeight += thingList.get(indexList.get(i+j)).getWeight();
    		somCost += thingList.get(indexList.get(i+j)).getCost();
    	}
    	if(somWeight <= NmaxWeight){
	    	if((somCost > maxCost) || ((somCost == maxCost) && (somWeight <= maxWeight))) {		    	
	    		indexSolution = i;
	    		maxWeight = somWeight;
	    		maxCost = somCost;
	    	}
    	}
    }
	for(int k = indexSolution; k < index + indexSolution; k++) {
	
		returnData += indexList.get(k) +",";
	}
    return returnData + maxCost +","+ maxWeight;
}

static void makeCombination(int array[], int data[],List<Integer> res, int start, int index) {

    if (index == data.length) {
    
        for (int j = 0; j < data.length; j++){
        	res.add(data[j]);
        }
        return;
    }
    for (int i = start; i < array.length && array.length-i >= data.length-index; i++) {
    
        data[index] = array[i];
        makeCombination(array, data,res, i+1, index+1);
    }
}

}
