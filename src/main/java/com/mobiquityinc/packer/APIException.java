package com.mobiquityinc.packer;

/**
 * 	Package	Challenge
 *  APIException Class
 *
 */
public class APIException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4547674584324114192L;

	public APIException(){
		super();
	}
	
	public APIException(String message, Throwable cause) {
		super(message, cause);
		
	}

	
}
